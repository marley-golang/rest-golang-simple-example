# Restful GO (Golang) Example

**Ссылка на оригинал:**  
https://github.com/codixir/books-list

<br/>

### GET ALL (GET)

    $ curl localhost:8000/books
    [{"ID":1,"Title":"Golang pointers","Author":"Mr. Golang","Year":"2010"},{"ID":2,"Title":"Goroutines","Author":"Mr. Goroutine","Year":"2011"},{"ID":3,"Title":"Golang routers","Author":"Mr. Router","Year":"2012"},{"ID":4,"Title":"Golang concurrency","Author":"Mr. Currency","Year":"2013"},{"ID":5,"Title":"Golang good parts","Author":"Mr. Good","Year":"2014"}]

<br/>

### GET BY ID (GET)

    $ curl localhost:8000/books/2

<br/>

### ADD NEW (POST)

    $ curl localhost:8000/books -X POST -H "Content-Type: application/json" -d '{"id":6, "title":"C++ is old", "author": "Mr. C++", "year": "2014"}'

<br/>

### UPDATE BY ID (PUT)

    $ curl localhost:8000/books -X PUT -H "Content-Type: application/json" -d '{"id":3, "title":"Golang is wonderful", "author": "Mr. Wonderful", "year": "2012"}'

<br/>

### DELETE BY ID (DELETE)

    $ curl localhost:8000/books/3 -X DELETE

<br/>

### CREATE POSTGRESQL DATABASE (elephantsql.com)

    CREATE TABLE books (id serial, title varchar, author varchar, year varchar);
    INSERT INTO books (title, author, year) values ('Golang is great', 'Mr. Great', '2012');
    INSERT INTO books (title, author, year) values ('C++ is greates', 'Mr. C++', '2015');
    SELECT * FROM books;

<br/>

### Additional packages

    $ go get github.com/lib/pq
    $ go get github.com/subosito/gotenv
